package com.au_fil_de_l_eau.android.au_fil_de_l_eau;

import com.google.gson.annotations.SerializedName;

public class Bookmark {

    @SerializedName("identifian")
    private int id;
    @SerializedName("bookmark")
    private Boolean bookmark;


    public Bookmark() {
    }

    Bookmark(int id, Boolean isBookmark) {
        this.id = id;
        this.bookmark = isBookmark;
    }

    int getId() {
        return id;
    }

    void setId(int id) {
        this.id = id;
    }


    public Boolean isBookmark() {
        return bookmark;
    }

    void setIsBookmark(Boolean bookmark) {
        this.bookmark = bookmark;
    }

    @Override
    public String toString() {
        return "Bookmark{" +
                "id=" + id +
                ", bookmark=" + bookmark +
                '}';
    }
}
