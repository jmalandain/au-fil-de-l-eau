package com.au_fil_de_l_eau.android.au_fil_de_l_eau;

import com.google.gson.annotations.SerializedName;

public class Lieu {

    @SerializedName("identifian")
    private int id;
    @SerializedName("elem_patri")
    private String nom="Pas de nom";
    @SerializedName("commune")
    private String ville="Pas de ville";
    @SerializedName("description")
    private String description="pas de description";
    @SerializedName("histo1")
    private String dateConstruction="Pas de date de construction";
    @SerializedName("elem_princ")
    private String elem_princ="Pas de d'élément de construction";
    @SerializedName("commentair")
    private String commentaire="pas de commentaire";


    public Lieu() {
    }

    Lieu(int id, String nom, String ville, String description, String dateConstruction, String elem_princ, String commentaire) {
        this.id = id;
        this.nom = nom;
        this.ville = ville;
        this.description = description;
        this.dateConstruction = dateConstruction;
        this.elem_princ = elem_princ;
        this.commentaire = commentaire;
    }

    int getId() {
        return id;
    }

    void setId(int id) {
        this.id = id;
    }

    String getDescription() {
        return description;
    }

    void setDescription(String description) {
        this.description = description;
    }

    String getDateConstruction() {
        return dateConstruction;
    }

    void setDateConstruction(String dateConstruction) {
        this.dateConstruction = dateConstruction;
    }

    String getElem_princ() {
        return elem_princ;
    }

    void setElem_princ(String elem_princ) {
        this.elem_princ = elem_princ;
    }

    String getNom() {
        return nom;
    }

    void setNom(String nom) {
        this.nom = nom;
    }

    String getVille() {
        return ville;
    }

    void setVille(String ville) {
        this.ville = ville;
    }

    String getCommentaire() {
        return commentaire;
    }

    void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }


}
