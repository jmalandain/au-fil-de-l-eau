package com.au_fil_de_l_eau.android.au_fil_de_l_eau;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class LieuAdapter extends ArrayAdapter<Lieu> implements View.OnClickListener{

    private ArrayList<Lieu> lieux;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtVille;
    }

    public LieuAdapter(ArrayList<Lieu> data, Context context) {
        super(context, R.layout.lieu_adapter, data);
        this.lieux = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        Lieu Lieu=(Lieu)object;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Lieu Lieu = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lieu_adapter, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.txtVille = (TextView) convertView.findViewById(R.id.ville);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }


        assert Lieu != null;
        viewHolder.txtName.setText(Lieu.getNom());
        viewHolder.txtVille.setText(Lieu.getVille());
        // Return the completed view to render on screen
        return result;
    }
}
