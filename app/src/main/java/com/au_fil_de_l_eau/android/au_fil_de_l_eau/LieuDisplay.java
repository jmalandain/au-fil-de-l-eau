package com.au_fil_de_l_eau.android.au_fil_de_l_eau;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class LieuDisplay extends AppCompatActivity {
    static final String FILE_NAME = "bookmarks.json";
    Lieu leLieu;
    TextView tvNom;
    TextView tvCommune;
    TextView tvDescript;
    TextView tvDateConstr;
    TextView tvPrinc;
    TextView tvComm;
    private Button buttonImage;
    private ImageView imageView;

    private static final int REQUEST_ID_READ_WRITE_PERMISSION = 99;
    private static final int REQUEST_ID_IMAGE_CAPTURE = 100;
    Button btFav;
    boolean bookmarked;

    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lieu_display);
        tvNom = (TextView) findViewById(R.id.tv_nom);
        tvCommune = (TextView) findViewById(R.id.tv_commune);
        tvDescript = (TextView) findViewById(R.id.tv_description);
        tvDateConstr = (TextView) findViewById(R.id.tv_dateConstr);
        tvPrinc = (TextView) findViewById(R.id.tv_princ);
        tvComm = (TextView) findViewById(R.id.tv_commentaire);
        btFav = (Button) findViewById(R.id.btn_fav);


        Intent i = getIntent();
        int idLieu = Integer.parseInt(i.getStringExtra("idLieu"));


        try {
            getLieu(this, idLieu);


            tvNom.setText(leLieu.getNom());
            tvCommune.setText(leLieu.getVille());
            tvDescript.setText(leLieu.getDescription());
            tvDateConstr.setText(leLieu.getDateConstruction());
            tvPrinc.setText(leLieu.getElem_princ());
            tvComm.setText(leLieu.getCommentaire());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.buttonImage = (Button) this.findViewById(R.id.button_image);
        this.imageView = (ImageView) this.findViewById(R.id.imageView);

        this.buttonImage.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage();
            }
        });
        isBookmarked(context);

        setButton();


        btFav.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                try {
                    ArrayList<Bookmark> bookmarks = listingBookmark(context);
                    ArrayList<Bookmark> newBookmmarks = new ArrayList<Bookmark>();
                    for (Bookmark bookmark : bookmarks) {
                        if (leLieu.getId() == bookmark.getId()) {
                            if (bookmarked) {
                                bookmark.setIsBookmark(false);
                                bookmarked = false;

                            } else {

                                bookmark.setIsBookmark(true);

                                bookmarked = true;
                            }
                        }
                        newBookmmarks.add(bookmark);

                    }

                    updateBookmarks(newBookmmarks);

                    setButton();


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });


    }


    @SuppressLint("SetTextI18n")
    private void setButton() {


        if (bookmarked) {
            btFav.setText("Retirer des favoris");
        } else {
            btFav.setText("Ajouter des favoris");
        }
    }


    private String jsonReader(Context context, String jsonToRead, int type) throws IOException, JSONException {
        String json = null;

        try {

            InputStream inputStream;
            if (type == 1) {

                inputStream = getAssets().open(jsonToRead);


            } else {
                inputStream = openFileInput(FILE_NAME);


            }
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    private void getLieu(Context context, int id) throws IOException, JSONException {
        String json = jsonReader(context, "short_patrimoine.json", 1);
        Gson gson = new Gson();

        Type lieuType = new TypeToken<ArrayList<Lieu>>() {
        }.getType();
        ArrayList<Lieu> lieux = gson.fromJson(json, lieuType);
        for (Lieu lieu : lieux) {
            if (lieu.getId() == id) {
                leLieu = lieu;
            }
        }
    }

    private ArrayList<Bookmark> listingBookmark(Context context) throws IOException, JSONException {
        String json = jsonReader(context, "bookmarks.json", 0);
        Gson gson = new Gson();

        Type bookmarksType = new TypeToken<ArrayList<Bookmark>>() {
        }.getType();
        ArrayList<Bookmark> bookmarks = gson.fromJson(json, bookmarksType);
        return bookmarks;
    }

    private void isBookmarked(Context context) {

        ArrayList<Bookmark> bookmarks = null;
        try {
            bookmarks = listingBookmark(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        assert bookmarks != null;
        for (Bookmark bookmark : bookmarks) {
            if (leLieu.getId() == bookmark.getId()) {
                bookmarked = bookmark.isBookmark();
            }
        }
        bookmarked = false;
    }

    private void updateBookmarks(ArrayList<Bookmark> bookmarks) throws IOException, JSONException {
        Gson gson = new Gson();

        String parsedJson = gson.toJson(bookmarks);



        File file = new File(FILE_NAME);
        if (!file.exists()) {


            FileOutputStream fos = null;
            try {
                fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
                fos.write(parsedJson.getBytes());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    private void captureImage() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        this.startActivityForResult(intent, REQUEST_ID_IMAGE_CAPTURE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ID_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                Bitmap bp = (Bitmap) data.getExtras().get("data");
                this.imageView.setImageBitmap(bp);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Action canceled", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Action Failed", Toast.LENGTH_LONG).show();
            }
        }
    }


}
