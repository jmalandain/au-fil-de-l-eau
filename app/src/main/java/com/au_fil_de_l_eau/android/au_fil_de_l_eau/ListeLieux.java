package com.au_fil_de_l_eau.android.au_fil_de_l_eau;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class ListeLieux extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    static final String FILE_NAME = "bookmarks.json";


    String[] villes;


    ArrayList<Lieu> lieux = new ArrayList<>();

    Spinner villeButton;
    Button favorisButton;
    Button nofiltreButton;
    ListView listView;
    Gson gson = new Gson();
    Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_lieux);
        final String PREFS_NAME = "MyPrefsFile";

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        if (settings.getBoolean("my_first_time", true)) {
            //the app is being launched for first time, do something
            Log.d("Comments", "First time");

            // first time task
            File file = new File(FILE_NAME);
            String text = null;
            try {
                text = gson.toJson(listingBookmark(context, 1));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            FileOutputStream fos = null;
            try {
                fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
                fos.write(text.getBytes());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            // record the fact that the app has been started at least once
            settings.edit().putBoolean("my_first_time", false).commit();
        }


        villeButton = (Spinner) findViewById(R.id.btn_ville);
        try {
            villes = listingVilleStr(context);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, villes);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            villeButton.setAdapter(adapter);
            villeButton.setOnItemSelectedListener(this);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        favorisButton = (Button) findViewById(R.id.btn_favoris);
        nofiltreButton = (Button) findViewById(R.id.btn_nofiltre);
        listView = (ListView) findViewById(R.id.list);


        try {
            lieux = listingLieu(context);
            adapterSetter(lieux);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        favorisButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                lieux = null;
                try {
                    lieux = lieuxByBookmark(context);

                    adapterSetter(lieux);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        nofiltreButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                lieux = null;
                try {
                    lieux = listingLieu(context);
                    adapterSetter(lieux);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // selected item
                Lieu lieu = (Lieu) parent.getItemAtPosition(position);

                Intent i = new Intent(getApplicationContext(), LieuDisplay.class);
                i.putExtra("idLieu", "" + lieu.getId());
                startActivity(i);

            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        if (position == 0) {
            lieux = null;
            try {
                lieux = listingLieu(context);
                adapterSetter(lieux);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                lieux = lieuxByVille(context, villes[position]);

                adapterSetter(lieux);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        lieux = null;
        try {
            lieux = listingLieu(context);
            adapterSetter(lieux);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void adapterSetter(ArrayList<Lieu> lieux) {
        LieuAdapter adapter;


        adapter = new LieuAdapter(lieux, getApplicationContext());


        listView.setAdapter(adapter);
    }

    private String jsonReader(Context context, String jsonToRead, int type) throws IOException, JSONException {
        String json = null;
        try {

            InputStream inputStream;
            if (type == 1) {

                inputStream = getAssets().open(jsonToRead);
            } else {
                inputStream = openFileInput(FILE_NAME);

            }

            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    private String[] listingVilleStr(Context context) throws IOException, JSONException {
        String json = jsonReader(context, "city.json", 1);


        Type villeType = new TypeToken<ArrayList<Ville>>() {
        }.getType();
        ArrayList<Ville> lesVilles = gson.fromJson(json, villeType);
        String[] villes = new String[lesVilles.size() + 1];
        int i = 1;
        villes[0] = "villes";
        for (Ville ville : lesVilles) {
            villes[i] = ville.getCommune();
            i++;
        }

        return villes;
    }

    private ArrayList<Lieu> listingLieu(Context context) throws IOException, JSONException {
        String json = jsonReader(context, "short_patrimoine.json", 1);
        Gson gson = new Gson();

        Type lieuType = new TypeToken<ArrayList<Lieu>>() {
        }.getType();
        ArrayList<Lieu> lieux = gson.fromJson(json, lieuType);
        return lieux;
    }

    private ArrayList<Lieu> lieuxByVille(Context context, String laVille) throws IOException, JSONException {
        ArrayList<Lieu> lieuxComplet = listingLieu(context);
        ArrayList<Lieu> lieux = new ArrayList<>();
        for (Lieu lieu : lieuxComplet) {
            if (lieu.getVille().equals(laVille)) {
                lieux.add(lieu);
            }
        }

        return lieux;

    }

    private ArrayList<Bookmark> listingBookmark(Context context, int type) throws IOException, JSONException {
        String json = jsonReader(context, "bookmarks.json", type);
        Gson gson = new Gson();

        Type bookmarksType = new TypeToken<ArrayList<Bookmark>>() {
        }.getType();
        ArrayList<Bookmark> bookmarks = gson.fromJson(json, bookmarksType);
        return bookmarks;
    }

    private ArrayList<Lieu> lieuxByBookmark(Context context) throws IOException, JSONException {
        ArrayList<Lieu> lieuxComplet = listingLieu(context);
        ArrayList<Bookmark> bookmarks = listingBookmark(context, 0);
        ArrayList<Lieu> lieux = new ArrayList<>();
        for (Lieu lieu : lieuxComplet) {
            for (Bookmark bookmark : bookmarks) {
                if (lieu.getId() == bookmark.getId() && bookmark.isBookmark()) {
                    lieux.add(lieu);

                }
            }
        }

        return lieux;

    }


}
