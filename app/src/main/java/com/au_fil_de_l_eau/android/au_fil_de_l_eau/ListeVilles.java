package com.au_fil_de_l_eau.android.au_fil_de_l_eau;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class ListeVilles extends AppCompatActivity {

    private String laVille;

    ArrayList<Ville> villes = new ArrayList<>();
    Context context;
    private ListView listVilles;
    private ArrayList<Ville> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_villes);

        listVilles = (ListView) findViewById(R.id.listVilles);
        items = new ArrayList<Ville>();

        try {
            Log.i("start", "listing");
            villes = listingVilles(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VilleAdapter adapter;

        adapter = new VilleAdapter(villes, getApplicationContext());

        listVilles.setAdapter(adapter);

    }

    private String jsonReader(Context context) throws IOException, JSONException {
        String json = null;
        try {
            InputStream inputStream = getAssets().open("city.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }


    private ArrayList<Ville> listingVilles(Context context) throws IOException, JSONException {
        String json = jsonReader(context);
        Gson gson = new Gson();

        Type VilleType = new TypeToken<ArrayList<Ville>>() {
        }.getType();
        ArrayList<Ville> villes = gson.fromJson(json, VilleType);
        return villes;

    }
}
