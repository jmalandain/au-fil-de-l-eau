package com.au_fil_de_l_eau.android.au_fil_de_l_eau;

import com.google.gson.annotations.SerializedName;


public class Ville {
    @SerializedName("commune")
    private String commune;
    @SerializedName("nb")
    private String nb;


    public Ville() {
        super();
        this.commune = "";
        this.nb = "";
    }

    public Ville(String commune, String nb) {
        super();
        this.commune = commune;
        this.nb = nb;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getNb() {
        return nb;
    }

    public void setNb(String nb) {
        this.nb = nb;
    }

}
