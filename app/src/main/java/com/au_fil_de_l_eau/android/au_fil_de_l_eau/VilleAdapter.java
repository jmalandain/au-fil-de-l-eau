package com.au_fil_de_l_eau.android.au_fil_de_l_eau;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class VilleAdapter extends ArrayAdapter<Ville> implements View.OnClickListener{

//    private Activity activity;
//    private List<Ville> items;
//    private Ville objBean;
//    private int row;
//
//    public VilleAdapter(Activity act, int resource, List<Ville> arrayList) {
//        super(act, resource, arrayList);
//        this.activity = act;
//        this.row = resource;
//        this.items = arrayList;
//    }
//
//    @ Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        View view = convertView;
//        ViewHolder holder;
//        if (view == null) {
//            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            view = inflater.inflate(row, null);
//            holder = new ViewHolder();
//            view.setTag(holder);
//        } else {
//            holder = (ViewHolder) view.getTag();
//        }
//        if ((items == null) || ((position + 1) > items.size())) return view;
//        objBean = items.get(position);
//        holder.commune = (TextView) view.findViewById(R.id.commune);
//        holder.nb = (TextView) view.findViewById(R.id.nb);
//        if (holder.commune != null && null != objBean.getCommune() && objBean.getCommune().trim().length() > 0) {
//            holder.commune.setText(Html.fromHtml(objBean.getCommune()));
//        }
//        if (holder.nb != null && null != objBean.getNb() && objBean.getNb().trim().length() > 0) {
//            holder.nb.setText(Html.fromHtml(objBean.getNb()));
//        }
//        return view;
//    }
//
//    public class ViewHolder {
//        public TextView commune, nb;
//    }

    private ArrayList<Ville> villes;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView commune;
        TextView nb;
    }

    public VilleAdapter(ArrayList<Ville> data, Context context) {
        super(context, R.layout.adapter_ville, data);
        this.villes = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        Ville ville=(Ville)object;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Ville Ville = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        VilleAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new VilleAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.adapter_ville, parent, false);
            viewHolder.commune = (TextView) convertView.findViewById(R.id.commune);
            viewHolder.nb = (TextView) convertView.findViewById(R.id.nb);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (VilleAdapter.ViewHolder) convertView.getTag();
            result=convertView;
        }


        assert Ville != null;
        viewHolder.commune.setText(Ville.getCommune());
        viewHolder.nb.setText(Ville.getNb());
        // Return the completed view to render on screen
        return result;
    }
}